#include "auxiliary.h"

#include <sstream>

std::string string_to_hex(const std::string &input) {
    static const char hex_digits[] = "0123456789ABCDEF";

    std::string output;
    output.reserve(input.length() * 2);
    for (unsigned char c : input)
    {
        output.push_back(hex_digits[c >> 4]);
        output.push_back(hex_digits[c & 15]);
    }
    return output;
}

std::vector<int> u16string_to_hex(const std::u16string &input) {
    std::basic_stringstream<char16_t> wss;
    wss << input;
    std::vector<int> hex_str;
    for (int v; wss >> std::hex >> v; ){
        hex_str.push_back(v);
    }
    return hex_str;
}
